import { RouterModule, Routes} from '@angular/router' ;
import { APPLICATION_MODULE_PROVIDERS } from '@angular/core/src/application_module';
import { CompraComponent } from './Components/compra/compra.component';
import { VentaComponent } from './Components/venta/venta.component';

const APP_ROUTES: Routes = [
    {path: 'Compra', component: CompraComponent},
    {path: 'Venta', component: VentaComponent},
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
