import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styles: []
})

export class CompraComponent implements OnInit {
  dolar = 18.50;
  peso = 1;
  euro = 21.20;

  grd = 30.82;
  grm = 589.50;
  gre = 27.20;
  constructor() { }

  ngOnInit() {
  }

  Divisa(cant: string, divi: string) {
    let u: number;
    let m: number;
    let e: number;
    // tslint:disable-next-line:radix
    let mon = parseFloat(cant);

    if (divi === 'MXN') {
      e = mon / 21.20;
      u = mon / 18.50;
      this.peso = mon;
      this.euro = e;
      this.dolar = u;
    }
    if (divi === 'USD') {
      m = mon * 18.50;
      e = mon * 0.88;
      this.dolar = mon;
      this.euro = e;
      this.peso = m;
    }
    if (divi === 'EUR') {
      u = mon * 1.13;
      m = mon * 21.20;
      this.euro = mon;
      this.dolar = u;
      this.peso = m;
    }
  }

  Oro(canto: string) {
    let a = parseFloat(canto);
    let grdl = 30.82;
    let grmx = 589.50;
    let greu = 27.20;
    this.grd = a * grdl;
    this.grm = a * grmx;
    this.gre = a * greu;
  }
}
