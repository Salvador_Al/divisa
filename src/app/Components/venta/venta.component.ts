import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styles: []
})
export class VentaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  dolar = 19.60;
  peso = 1;
  euro = 22.50;

  grd = 51.36;
  grm = 982.36;
  gre = 45.34;


  Divisa(cant: string, divi: string) {
    let u: number;
    let m: number;
    let e: number;
    // tslint:disable-next-line:radix
    let mon = parseFloat(cant);

    if (divi === 'MXN') {
      e = mon / 22.50;
      u = mon / 19.60;
      this.peso = mon;
      this.euro = e;
      this.dolar = u;
    }
    if (divi === 'USD') {
      m = mon * 19.60;
      e = mon * 0.88;
      this.dolar = mon;
      this.euro = e;
      this.peso = m;
    }
    if (divi === 'EUR') {
      u = mon * 1.13;
      m = mon * 22.50;
      this.euro = mon;
      this.dolar = u;
      this.peso = m;
    }
  }

  Oro(canto: string) {
    let a = parseFloat(canto);
    let grdl = 51.36;
    let grmx = 982.36;
    let greu = 45.34;
    this.grd = a * grdl;
    this.grm = a * grmx;
    this.gre = a * greu;
  }
}
